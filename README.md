# Make cheat sheet

A dummie cheat sheet for Make, the rules runner widely avaivable in Unix/GNU distros

## Getting started

Create a Makefile named : `Makefile` and edit it with __tab indentation__

## Create a rule

```make
[targets]: [prerequisites]
    [recipe]
```

example:


```make
build: packages.json
    npm run build
```

## Fakes rules

For create _fakes_ rules use `.PHONY: [rule1] [rule2] ...` at the begin of Makefile


```make
.PHONY: test

build: packages.json
    npm run build

test: 
    npm run test
```

## help rule

Help users by commment yours rules and create this task:

```make
.DEFAULT_GOAL: help ##default task

help: 
    @grep -E '(^[a-zA-Z_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[32m%-10s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'

test: ## Lance les tests
    npm run test
```

## Rule with parameters

For using parameters create variables with 

```make
MYVARIABLE?= 443 ##default value if not declared

server:
    npm run watch --port ${MYVARIABLE}
```

## Reference cheat sheet

- [https://devhints.io/makefile](https://devhints.io/makefile)
